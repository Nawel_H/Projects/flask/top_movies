Projet TOP films

https://data.toulouse-metropole.fr/explore/dataset/top-500-des-films-les-plus-empruntes-a-la-bibliotheque-de-toulouse/information/
Le citoyen effectue une recherche textuelle pour vérifier si le titre a été un des films les plus empruntés.


Par exemple : S’il recherche “Harry”, cela doit lui retourner tous les films comprenant “Harry” dans le titre
